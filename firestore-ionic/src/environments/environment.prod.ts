export const environment = {
  production: true,
  firebase: {
      apiKey: "XXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXX",
      authDomain: "XXXXXXXXX-XXXXX-XXXXX.XXXXXXXXXXX.XXX",
      databaseURL: "https://XXXXXXXXX-XXXXX-XXXXX.firebaseio.com",
      projectId: "XXXXXXXXX-XXXXX-XXXXX",
      storageBucket: "XXXXXXXXX-XXXXX-XXXXX.appspot.com",
      messagingSenderId: "XXXXXXXXXXXX"
    }
};
