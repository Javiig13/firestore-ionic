import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  private subjectUsuario: Subject<object> = new Subject<object>();
  constructor(private firestore: AngularFirestore) { }

  //Crear Usuario
  public crearUsuario(data: { nombre: string, apellidos: string, telefono: string }) {
    return this.firestore.collection('usuarios').add(data);
  }

  //Obtener Usuario
  public obtenerUsuario(documentId: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
       this.firestore.collection('usuarios').doc(documentId).get().subscribe((doc:any) => {
         let usuario: any = {
           id: doc.id,
           data: doc.data()
         }
         resolve(usuario);
       }, (error) => {
         reject(error);
       });
    })
  }

  //Obtener Usuarios
  public obtenerUsuarios(): Observable<any[]> {
    return this.firestore.collection('usuarios').snapshotChanges();
  }

  //Actualizar Usuario
  public actualizarUsuario(documentId: string, data: any) {
    return this.firestore.collection('usuarios').doc(documentId).set(data);
  }

  //Borrar Usuario
  public borrarUsuario(documentId: string) {
    return this.firestore.collection('usuarios').doc(documentId).delete();
  }
}
