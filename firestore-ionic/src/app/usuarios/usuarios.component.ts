import { Component, OnInit } from '@angular/core';
import { FirestoreService } from '../services/firestore/firestore.service';
import { EditarUsuarioPage } from '../paginas/editar-usuario/editar-usuario.page';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss'],
})
export class UsuariosComponent implements OnInit {
  paginaEdicionUsuario: any;
  public usuarios: any[] = [];

  constructor(private firestoreService: FirestoreService,
    private alertCtrl: AlertController) {
    this.paginaEdicionUsuario = EditarUsuarioPage;
  }

  ngOnInit() {
    this.firestoreService.obtenerUsuarios().subscribe((usuariosSnapshot) => {
      usuariosSnapshot.forEach((datosUsuario: any) => {
        this.usuarios.push({
          id: datosUsuario.payload.doc.id,
          data: datosUsuario.payload.doc.data()
        });
      })
    }, (error) => {
      console.log(`Ha habido un problema al cargar los usuarios: ${error}`);
      this.mostrarMensaje('Ha habido un problema al cargar los usuarios');
    });
  }

  public borrarUsuario(IdDocumento: string): void {
    this.usuarios = [];
    this.firestoreService.borrarUsuario(IdDocumento).then(() => {
      console.log('Se ha borrado correctamente el usuario');
      this.mostrarMensaje('Se ha borrado correctamente el usuario');
    }).catch((error) => {
      console.log(`Ha habido un problema borrando el usuario: ${error}`);
      this.mostrarMensaje('Ha habido un problema borrando el usuario');
    })
  }

  async mostrarMensaje(mensaje: string) {
    const alert = await this.alertCtrl.create({
      header: `${mensaje}`,
      buttons: ['Aceptar']
    });
    await alert.present();
  }
}
