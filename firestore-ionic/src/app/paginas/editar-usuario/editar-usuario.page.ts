import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.page.html',
  styleUrls: ['./editar-usuario.page.scss'],
})
export class EditarUsuarioPage implements OnInit {
  private idDocumento: string;
  usuario: any = {};

  constructor(private route: ActivatedRoute,
    private navCtrl: NavController,
    private firestoreService: FirestoreService,
    private alertCtrl: AlertController) { }

  ngOnInit() {
    this.idDocumento = this.route.snapshot.params.id;
    this.firestoreService.obtenerUsuario(this.idDocumento).then((resultado: any) => {
      this.usuario = {
        id: resultado.id,
        nombre: resultado.data.nombre,
        apellidos: resultado.data.apellidos,
        telefono: resultado.data.telefono
      }
    }).catch((error) => {
      console.log(`No se ha podido obtener el documento, ${error}`);
      this.mostrarMensaje('No se pueden obtener los datos del usuario');
    });
  }

  retroceder() {
    this.navCtrl.back();
  }
  
  actualizarUsuario(): void {
    let usuarioActualizado = {
      id: this.usuario.id,
      nombre: this.usuario.nombre,
      apellidos: this.usuario.apellidos,
      telefono: this.usuario.telefono
    }
    this.firestoreService.actualizarUsuario(this.usuario.id, usuarioActualizado).then(() => {
      console.log('Usuario actualizado correctamente');
      this.mostrarMensaje('Datos actualizados');
    }).catch((error) => {
      console.log(`Ha habido un problema actualizando el usuario, ${error}`);
      this.mostrarMensaje('Ha habido un problema actualizando el usuario');
    });
  }

  async mostrarMensaje(mensaje: string) {
    const alert = await this.alertCtrl.create({
      message: `${this.usuario.nombre} ${this.usuario.apellidos}`,
      header: `${mensaje}`,
      buttons: [{ text: 'Aceptar', handler: () => { this.navCtrl.back() } }]
    });
    await alert.present();
  }
}
