import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { FirestoreService } from 'src/app/services/firestore/firestore.service';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.page.html',
  styleUrls: ['./crear-usuario.page.scss'],
})
export class CrearUsuarioPage implements OnInit {

  constructor(private navCtrl: NavController,
    private firestoreService: FirestoreService,
    private alertCtrl: AlertController) { }

  nombre: string;
  apellidos: string;
  telefono: string;
  nuevoUsuario: any = {}

  ngOnInit() {
  }

  retroceder() {
    this.navCtrl.back();
  }

  guardarUsuario() {
    this.nuevoUsuario = {
      nombre: this.nombre,
      apellidos: this.apellidos,
      telefono: this.telefono
    }
    this.firestoreService.crearUsuario(this.nuevoUsuario).then(() => {
      console.log('Usuario guardado correctamente.');
      this.mostrarMensaje('Usuario guardado correctamente');
    }).catch((error) => {
      console.log(`Ha habido un problema guardando el usuario ${error}`);
      this.mostrarMensaje('Hubo un problema guardando el usuario');
    });
  }

  async mostrarMensaje(mensaje: string) {
    const alert = await this.alertCtrl.create({
      message: `${this.nuevoUsuario.nombre} ${this.nuevoUsuario.apellidos}`,
      header: `${mensaje}`,
      buttons: [{ text: 'Aceptar', handler: () => { this.navCtrl.back() } }]
    });
    await alert.present();
  }
}
