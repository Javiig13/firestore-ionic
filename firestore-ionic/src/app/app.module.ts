import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { FirestoreService } from './services/firestore/firestore.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { EditarUsuarioPage } from './paginas/editar-usuario/editar-usuario.page';
import { FormsModule } from '@angular/forms';
import { CrearUsuarioPage } from './paginas/crear-usuario/crear-usuario.page';

@NgModule({
  declarations: [
    CrearUsuarioPage,
    EditarUsuarioPage,
    AppComponent,
    UsuariosComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    RouterModule.forRoot([
      { path: '', redirectTo: 'usuarios', pathMatch: 'full' },
      { path: 'usuarios', component: UsuariosComponent },
      { path: 'editar-usuario/:id', component: EditarUsuarioPage },
      { path: 'crear-usuario', component: CrearUsuarioPage }
    ]),
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFirestore,
    FirestoreService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
